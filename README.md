# rcc-server

This Application is the Online Service for a remote controlled RC Car.

### Setup

```
yarn install
```

### Commit Conventions

- feat: new feature
- fix: a bug fix
- refactor: A code change that neither fixes a bug or adds a feature
- perf: A code change that improves performance
- test: Adding missing tests
- chore: Chnages to the build process or auxiliary tools and libraries such as documentation generation


$(function() {
  //var socket = io.connect('http://localhost:8080');
  var socket = io.connect('https://remote-car-server.herokuapp.com');
  socket.emit('type', 'remote');


  socket.on('peng', function(data) {
    console.log(new Date() - new Date(data));
  })

  if (!(window.DeviceMotionEvent)) {
    $('.selector-motion').remove();
    $('.remote_motion').remove();
  }
  if (mobileAndTabletcheck()) {
    $('.remote_keyboard').remove();
    $('.selector-keyboard').remove();
  }

  $('button').show();

  $('button').click(function() {
    var type = $(this).data('type');
    switch (type) {
      case 'keyboard':
        $('.remote_motion').remove();
        $('.remote_joystick').remove();
        useKeyboard();
        break;
      case 'joystick':
        $('.remote_motion').remove();
        $('.remote_keyboard').remove();
        useJoystick();
        break;
      case 'motion':
        $('.remote_joystick').remove();
        $('.remote_keyboard').remove();
        useMotion()
        break;
      case 'ping':
        socket.emit('pang', new Date());
        break;
    }
    if (type != 'ping') {
      $('.typeselector').remove();
      $('.remote').show();
    }

  })

  var angle = 0;
  var accelerator = 0;
  var inputType = 'motion';


  function useJoystick() {
    var interval;
    var joystick = new VirtualJoystick({
      container: $('.remote_joystick')[0],
      mouseSupport: true,
      limitStickTravel: true,
      stickRadius: 100
    });

    $('.remote_joystick').on('mousedown', function() {
      interval = setInterval(function() {
        sendDrivingData(Math.round(joystick.deltaX()), Math.round(joystick.deltaY()));
      }, 60);
    });

    $('.remote_joystick').on('mouseup', function() {
      clearInterval(interval);
      sendDrivingData(0, 0);
    });

  }

  function useKeyboard() {
    $(document).on('keydown', function(e) {
      if (e.keyCode > 36 && e.keyCode < 41) {
        switch (e.keyCode) {
          case 37: //left
            angle = 80;
            $('.left').show();
            break;
          case 39: //right
            angle = -80;
            $('.right').show();
            break;
          case 38: //forwards
            accelerator = 50;
            $('.up').show();
            break;
          case 40: //backwards
            accelerator = -50;
            $('.down').show();
        }
        sendDrivingData(angle, accelerator);
      }
    });

    $(document).on('keyup', function(e) {
      if (e.keyCode > 36 && e.keyCode < 41) {
        switch (e.keyCode) {
          case 37: //left
            angle = 0;
            $('.left').hide();
            break;
          case 39: //right
            angle = 0;
            $('.right').hide();
            break;
          case 38: //forwards
            accelerator = 0;
            $('.up').hide();
            break;
          case 40: //backwards
            accelerator = 0;
            $('.down').hide();
        }

        sendDrivingData(angle, accelerator);
      }
    });



  }

  function useMotion() {
    window.addEventListener('devicemotion', handleMotion);

    $('.remote_motion').click(function() {
      angle = 0;
      accelerator = 0;
      sendDrivingData(Math.floor(angle), Math.floor(accelerator));
    })

    function handleMotion(e) {

      var steering = e.rotationRate.gamma;
      var gas = e.rotationRate.beta;

      var oldangle = angle;
      angle = clamp(angle + (steering * 2), -100, 100);
      var oldaccelerator = accelerator;
      accelerator = clamp(accelerator + (gas * 2), -100, 100);

      if (Math.abs(oldangle - angle) > 0.5 || Math.abs(oldaccelerator - accelerator) > 0.5) {
        sendDrivingData(Math.floor(angle), Math.floor(accelerator));
      }
    }

    function clamp(number, min, max) {
      if (number > max) {
        return max;
      } else if (number < min) {
        return min;
      } else {
        return number;
      }
    }
  }

  function sendDrivingData(currentAngle, currentAcceleration) {
    socket.emit('command', {
      angle: currentAngle,
      accelerator: currentAcceleration
    });
  }

  function mobileAndTabletcheck() {
    var check = false;
    (function(a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
  };


});

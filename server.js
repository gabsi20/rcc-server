'use strict'

const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 8080

let remote, vehicle;

server.listen(port, function() {
  console.log("server listening on port " + port)
});

app.all('*', function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*'); // allow acces from frontend server
  next();
});

app.use(express.static('client'));

io.on('connection', function(socket) {
  console.log("client connected");

  socket.on('type', function(type) {
    if (type == 'remote') {
      remote = socket;
    }
    if (type == 'vehicle') {
      vehicle = socket;
    }
  })

  socket.on('command', function(data) {
    if (vehicle) {
      vehicle.emit('control', data);
    }
  });

  socket.on('pang', function(data) {
    vehicle.emit('pang', data);
  });

  socket.on('peng', function(data) {
    remote.emit('peng', data);
  });

});
